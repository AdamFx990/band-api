namespace BandApi.Interfaces;

public interface ICommand
{
    public Task Fire();
}
public interface ICommand<T>
{
    public Task<T> Fire();
}
