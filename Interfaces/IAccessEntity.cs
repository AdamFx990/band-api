namespace BandApi.Interfaces;

public interface IAccessEntity : IEntity
{
    public DateTime Modified { get; set; }
    public DateTime Created { get; set; }
    public DateTime Accessed { get; set; }
    
}