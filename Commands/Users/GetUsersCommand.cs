using BandApi.Interfaces;
using BandApi.Contexts;
using BandApi.Models;

namespace BandApi.Commands.Users;
public class GetUsersCommand : ICommand<IEnumerable<User>>
{
    public string Email { get; set; }
    public async Task<IEnumerable<User>> Fire()
    {
        using (var context = new BandContext())
        {
            var entities = context.Users.ToList();

            foreach(var user in entities)
            {
                user.Accessed = DateTime.UtcNow;
            }
            await context.SaveChangesAsync();

            return entities;
        }
        
    }
}