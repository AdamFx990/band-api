using BandApi.Interfaces;
using BandApi.Contexts;
using BandApi.Models;

namespace BandApi.Commands.Users;
public class GetUserCommand : ICommand<User>
{
    public string Email { get; set; }
    public async Task<User> Fire()
    {
        using (var context = new BandContext())
        {
            var user = context.Users.FirstOrDefault((u) => u.Email == Email);
            if (user != null) 
            {
                // Success
                return user;
            }
            // Failure
            var ex = new Exception($"User {Email} not found");
            throw ex;
        }
    }
}