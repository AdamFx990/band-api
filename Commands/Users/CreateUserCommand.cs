using BandApi.Interfaces;
using BandApi.Contexts;
using BandApi.Models;

namespace BandApi.Commands.Users;
public class CreateUserCommand : ICommand
{
    private readonly BandContext _context;
    public CreateUserCommand(BandContext context)
    {
        _context = context;
    }
    public string Email { get; set; }

    public async Task Fire()
    {
        #region validation
        if (!Email.Contains('@') || !Email.Contains('.'))
        {
            throw new Exception($"Invalid email address \"{Email}\"");
        }

        var user = _context.Users.FirstOrDefault((u) => u.Email == Email);
        if (user != null)
        {
            throw new Exception($"Cannot create a user with the email \"{Email}\"");
        }
        #endregion validation

        var entity = new User
        {
            Id = Guid.NewGuid(),
            Email = Email,
            Accessed = DateTime.UtcNow,
            Created = DateTime.UtcNow,
            Modified = DateTime.UtcNow
        };
        await _context.Users.AddAsync(entity);
        await _context.SaveChangesAsync();
    }
}