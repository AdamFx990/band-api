using Microsoft.AspNetCore.Mvc;
using BandApi.Commands.Users;
using BandApi.Contexts;

namespace BandApi.Controllers;
[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{
    private readonly ILogger<UserController> _logger;
    private readonly BandContext _context;

    public UserController(ILogger<UserController> logger, BandContext context)
    {
        _logger = logger;
        _context = context;
    }

    [HttpGet("{email}")]
    public async Task<IActionResult> Get(string email)
    {
        _logger.LogInformation($"Executing GET /user/{email}");
        var command = new GetUserCommand()
        {
            Email = email
        };
        var response = await command.Fire();
        return Ok(response);
    }

    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        _logger.LogInformation("Executing GET /user");
        var command = new GetUsersCommand();
        var response = await command.Fire();
        return Ok(response);
    }

    [HttpPost]
    public async Task<IActionResult> Post(CreateUserCommand command)
    {
        _logger.LogInformation("Executing POST /user");
        await command.Fire();
        return Ok();
    }
}
