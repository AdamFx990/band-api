using BandApi.Interfaces;

namespace BandApi.Models;
public class User : IAccessEntity
{
    public Guid Id { get; set; }
    public string Email { get; set; }
    public DateTime Accessed { get; set; }
    public DateTime Created { get; set; }
    public DateTime Modified { get; set; }
}

