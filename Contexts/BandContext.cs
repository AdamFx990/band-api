using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using BandApi.Models;

namespace BandApi.Contexts
{
    public class BandContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=192.168.1.4;Database=BandApi;Username=postgres;Password=postgres");
    }
}